﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;

namespace envirocheck2
{
    class WspWebClient : WebClient
    {
        private int _timeout;
        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }
        public void WebDownload()
        {
            this._timeout = 6000000;
        }
        public void WebDownload(int time)
        {
            this._timeout = time;
        }
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest result = base.GetWebRequest(address);
            result.Timeout = this._timeout;
            return result;
        }
    }

    class Program
    {
        static string GetLink(string text, int startSearch)
        {
            string downloadLink = System.Configuration.ConfigurationSettings.AppSettings["EnvirocheckEmailReportLinkSite"];
            // string downloadLink = "http://www.landmarkinfo.co.uk/apps/filedownload";
            
            if (startSearch < 0)
            {
                return "";
            }

            //*** Get the download link
            int linkStart = text.IndexOf(downloadLink, startSearch);
            int linkEnd = text.IndexOf(".zip", startSearch);

            string link = "";

            if (linkStart > -1 && linkEnd > linkStart)
            {
                int linkLength = linkEnd + 4 - linkStart;
                link = text.Substring(linkStart, linkLength);

                Console.WriteLine(link);

                linkEnd = linkStart + linkLength;

                bool invalidLink = false;

                List<byte> asciiBytes = Encoding.ASCII.GetBytes(link).ToList<byte>();

                int i = 0;
                foreach (char c in link)
                {
                    //Console.WriteLine(c.ToString() + " : " + asciiBytes[i].ToString());
                    if (i < asciiBytes.Count-1)
                    {
                        i++;
                        if (asciiBytes[i].ToString() == "0")
                        {
                            invalidLink = true;
                            break;
                        }
                    }
                }
                
                // If the link is invalid, keep looking
                if (invalidLink)
                {
                    link = GetLink(text, linkEnd);
                }
            }
            else
            {
                link = GetLink(text, linkStart);
            }
            
            return link;
        }

        static void Main(string[] args)
        {
            string folderPath = System.Configuration.ConfigurationSettings.AppSettings["EmailFolderPath"];
            string downloadPath = System.Configuration.ConfigurationSettings.AppSettings["DownloadFolderPath"];
            string strFileNotFoundText = System.Configuration.ConfigurationSettings.AppSettings["FileNotFoundText"];

            //string folderPath = "D:\\Envirocheck Emails\\2019";
            //string downloadPath = "D:\\Envirocheck Emails\\2019\\downloads";
            //string strFileNotFoundText = "Error retrieving file";

            List<string> logLines = new List<string>();

            // Get all the filenames
            string[] files = System.IO.Directory.GetFiles(folderPath);

            foreach (string file in files)
            {
                //*** Get the order number
                string fileName = System.IO.Path.GetFileName(file);
                string orderNum = System.Text.RegularExpressions.Regex.Match(fileName, @"\d\d\d\d\d\d\d\d\d").Value; // Order number is 9 numbers

                Console.WriteLine(fileName + " : " + orderNum);
                
                string mailText = System.IO.File.ReadAllText(file);

                //*** Get the download link
                string link = GetLink(mailText, 0);

                Console.WriteLine(link);
                
                if (link.Length > 0)
                { 
                    //*** Do the file download
                    string downloadFile = link.Substring(link.IndexOf("userFileName=") + 13);
                    
                    WspWebClient client = new WspWebClient();
                    client.WebDownload(2147483647);

                    //client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705;)");
                    client.Headers.Add("user-agent", "Mozilla / 5.0(Windows NT 10.0; Win64; x64; Xbox; Xbox One) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 92.0.4515.107 Safari / 537.36 Edge / 44.18363.8131");
                    byte[] data = client.DownloadData(link);
                     
                    // Get the set of data and convert to string
                    int iLength = strFileNotFoundText.Length;

                    string strData = System.Text.ASCIIEncoding.ASCII.GetString(data, 0, iLength);

                    if (strData != strFileNotFoundText)
                    {
                        // ** Write data to file
                        string strFilePath = downloadPath + "\\" + downloadFile;
                        if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(strFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(strFilePath));
                        }
                        System.IO.BinaryWriter binWriter = new System.IO.BinaryWriter(System.IO.File.Open(@"" + strFilePath, System.IO.FileMode.Create));
                        binWriter.Write(data);
                        binWriter.Close();

                        logLines.Add("** Successfully downloaded '" + downloadFile + "' from '" + fileName + "'");

                    }
                    else
                    {
                        logLines.Add("XX Cannot retrieve '" + downloadFile + "' from '" + fileName + "'");
                    }

                }
                else
                {
                    logLines.Add("!! No download link in '" + fileName + "'");
                }


            }

            string path = downloadPath + "\\download.log";
            string[] lines = logLines.ToArray();

            // This text is added only once to the file.
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                System.IO.File.WriteAllLines(path, lines, Encoding.UTF8);
            }

        }
    }
}
